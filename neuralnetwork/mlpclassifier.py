import numpy as np
import sklearn
from sklearn.neural_network import MLPClassifier as DNN
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score as cv
import matplotlib.pyplot as plt
from sklearn.datasets import load_breast_cancer
from sklearn.tree import DecisionTreeClassifier as DTC
from sklearn.model_selection import train_test_split as TTS
from time import time
import datetime

data = load_breast_cancer()
X = data.data
y = data.target
Xtrain, Xtest, Ytrain, Ytest = TTS(X, y, test_size=0.3, random_state=420)
times = time()
dnn = DNN(hidden_layer_sizes=(10, 10, 10), random_state=420, max_iter=1000)
score = cv(dnn, X, y, cv=5)
print(score)
print(score.mean())
print(time() - times)
# 使用决策树进行一个对比
times = time()
clf = DTC(random_state=420)
print(cv(clf, X, y, cv=5).mean())
print(time() - times)

dnn = DNN(hidden_layer_sizes=(100,), random_state=420, max_iter=1000).fit(Xtrain, Ytrain)
score1 = dnn.score(Xtest, Ytest)
print("100 :", score1)
# 使用重要参数n_layers_
dnn.n_layers_
# 可见，默认层数是三层，由于必须要有输入和输出层，所以默认其实就只有一层隐藏层

# 如果增加一个隐藏层上的神经元个数，会发生什么呢?
dnn = DNN(hidden_layer_sizes=(200,), random_state=420, max_iter=1000)

dnn = dnn.fit(Xtrain, Ytrain)
score2 = dnn.score(Xtest, Ytest)
print("200 :", score)
# 看似结果会
# 来试试看学习曲线
s = []
for i in range(100, 2000, 10):
    dnn = DNN(hidden_layer_sizes=(int(i),), random_state=420, max_iter=1000).fit(Xtrain, Ytrain)
    s.append(dnn.score(Xtest, Ytest))
print(len(s))
print(i, max(s))
plt.figure(figsize=(20, 5))
plt.plot(range(100, 2000, 10), s)
plt.show()

# 那如果增加隐藏层，控制神经元个数，会发生什么呢?
s = []

layers = [(100,), (100, 100), (100, 100, 100), (100, 100, 100, 100), (100, 100, 100, 100, 100),
          (100, 100, 100, 100, 100, 100)]

for i in layers:
    dnn = DNN(hidden_layer_sizes=(i), random_state=420, max_iter=1000).fit(Xtrain, Ytrain)
    s.append(dnn.score(Xtest, Ytest))
print(s)
print(i, max(s))
plt.figure(figsize=(20, 5))
plt.plot(range(3, 9), s)
plt.xticks([3, 4, 5, 6, 7, 8])
plt.xlabel("Total number of layers")
plt.show()

# 如果同时增加隐藏层和神经元个数，会发生什么呢?
s = []
layers = [(100,), (150, 150), (200, 200, 200), (300, 300, 300, 300)]
for i in layers:
    dnn = DNN(hidden_layer_sizes=(i), random_state=420, max_iter=1000).fit(Xtrain, Ytrain)
    s.append(dnn.score(Xtest, Ytest))
print(i, max(s))
plt.figure(figsize=(20, 5))
plt.plot(range(3, 7), s)
plt.xticks([3, 4, 5, 6])
plt.xlabel("Total number of layers")
plt.show()
