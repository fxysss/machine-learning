from sklearn import datasets
import numpy as np
from sklearn.neighbors import KNeighborsRegressor


def flowerPredict():
    # 获取数据
    iris = datasets.load_iris()
    data = np.array(iris.data)

    trainData = data[:120, :3]
    trainResult = data[:120, 3]

    testData = data[120:, :3]
    realResult = data[120:, 3]

    neigh = KNeighborsRegressor(n_neighbors=3, algorithm='auto')
    # 拟合模型, trainingMat为训练矩阵,hwLabels为对应的标签
    trainResult = [e for e in trainResult]
    print(realResult)
    neigh.fit(trainData, trainResult)

    result = neigh.predict(testData)

    index = 0
    for oneResult in result:
        print("预测值：%f--------真实值：%f -------误差率：%f" % (
            oneResult, realResult[index], abs(oneResult - realResult[index]) / realResult[index]))
        index += 1


if __name__ == '__main__':
    flowerPredict()
