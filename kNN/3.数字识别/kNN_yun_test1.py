import numpy as np
from os import listdir
from sklearn.neighbors import KNeighborsClassifier as kNN


def dealOneFile(rf):
    oneFile = np.zeros((1, 1024))
    for i in range(32):
        onlineData = rf.readline()
        onlineData = onlineData[:-1]
        for j in range(32):
            oneFile[0, i * 32 + j] = int(onlineData[j])
    return oneFile


def figureSee():
    # 测试集标签
    trainLabels = []
    # 训练集列表
    trainFileList = listdir("trainingDigits")
    # 训练集数量
    trainFileNum = len(trainFileList)

    trainMat = np.zeros((trainFileNum, 1024))
    for i in range(trainFileNum):
        fileName = trainFileList[i]
        # 添加标签
        trainLabels.append(int(fileName.split("_")[0]))
        rf = open("trainingDigits/" + fileName)
        trainMat[i] = dealOneFile(rf)

    neigh = kNN(n_neighbors=3, algorithm='auto')
    # 拟合模型, trainingMat为训练矩阵,hwLabels为对应的标签
    neigh.fit(trainMat, trainLabels)

    # 测试集列表
    testFileList = listdir("testDigits")
    testFileNum = len(testFileList)

    errorNum = 0.0
    for i in range(testFileNum):
        fileName = testFileList[i]
        # 添加标签
        realResult = int(fileName.split("_")[0])
        rf = open("testDigits/" + fileName)
        oneTest = dealOneFile(rf)
        result = neigh.predict(oneTest)
        #print(type(result),type(realResult))
        if realResult != result:
            errorNum += 1
            print("realResult!=result")

        print("真实结果:%d     预测结果：%d" % (realResult, result))

    print(errorNum)


if __name__ == '__main__':
    figureSee()
