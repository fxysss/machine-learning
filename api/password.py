#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
import zipfile


def zip_attack(file_name):
    file_handle = zipfile.ZipFile(file_name)
    handle_password = open('pwords.txt')
    for pwd in handle_password:
        pwd = pwd.rstrip()
        try:
            file_handle.extractal(path='test', pwd=pwd.encode())
            print('Found:' + pwd)
            break
        except:
            pass
    handle_password.close()
    file_handle.close()


if __name__ == '__main__':
    file_name = 'D:\文件\音乐\华晨宇 274首音乐+最新40首音乐打包下载.zip'
    if os.path.isfile(file_name) and file_name.endswith('.zip'):
        zip_attack(file_name)
    else:
        print('Not ZIP')
