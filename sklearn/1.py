# 引入数据
from sklearn import datasets
import numpy as np

iris = datasets.load_iris()
X = iris.data[:, [1, 3]]
y = iris.target
print("Class labels:", np.unique(y))  # 打印分类类别的种类